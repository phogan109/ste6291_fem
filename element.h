#ifndef ELEMENT_H
#define ELEMENT_H

#include "gmTrianglesystemModule"
#include "node.h"

template <typename T>
class Element
{
public:
    Element();

    void calculateStiffnessLoad();
    void invertStiffness();
    void calculateXVector(float force);
    void addNode(Node<T> newNode);
    void prepare();

    std::vector<Node<T>> getNodes();

    GMlib::DMatrix<T> getStiffnessMatrix();
    GMlib::DMatrix<T> getInvertedStiffnessMatrix();
    GMlib::DVector<T> getLoadVector();
    GMlib::DVector<T> getXVector();

private:
    GMlib::DMatrix<T> stiffnessMatrix;
    GMlib::DMatrix<T> invertedStiffnessMatrix;
    GMlib::DVector<T> loadVector;
    GMlib::DVector<T> xVector;
    std::vector<Node<T>> nodes;
    //GMlib::Array<GMlib::TSTriangle<T>> triangles;

};


#include "element.cpp"


#endif // ELEMENT_H
