#ifndef NODE_H
#define NODE_H

#include "gmTrianglesystemModule"
#include "memory"

template <typename T>
class Node
{
public:
    Node();
    Node( GMlib::TSVertex<T>* );
    ~Node();

    GMlib::TSVertex<T>           getVertex();
    GMlib::TSVertex<T>*          getVertexRef();
    void                         setVertex(GMlib::TSVertex<T>);
    void                         setZValue(T zValue);

    bool isNeighbor(Node<T> neighbor);
    GMlib::TSEdge<T>* getNeighborEdge(Node<T> neighbor);
    GMlib::ArrayLX<GMlib::TSVertex<T>*> getVectorVertices(GMlib::TSEdge<T>* edge);
    GMlib::ArrayLX<GMlib::TSVertex<T>*> getDiagonalVertices(GMlib::TSTriangle<T>* triangle);

private:
    GMlib::TSVertex<T>*         vertex;

};

#include "node.cpp"

#endif // NODE_H
