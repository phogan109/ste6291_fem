
#ifdef FEM_H


//REGULAR POINT GENERATOR FOR DRUM
template <typename T>
void Drum<T>::reg(int rings, int points, T radius)
{
    GMlib::SqMatrix<T,2> rotationMatrix;
    GMlib::Vector<T,2> rotationVector {radius, 0.0f};
    GMlib::Vector<T,2> workingVector;

    float ringF = 1.0f / rings;

    this->insertAlways(center);
    GMlib::TSVertex<T>* vertexPointer = this->getVertex(this->getSize() - 1);
    Node<T> centerNode = Node<T>(vertexPointer);
    this->addElement(centerNode);


    for (int i = 0; i < rings; i++)
    {
        workingVector = (float(i+1)*ringF) * rotationVector;
        rotationMatrix = GMlib::SqMatrix<T,2>( GMlib::Angle ( M_2PI / (points * std::pow(2, i)) ) );
        for (int k = 0; k < (points * pow(2, i)); k ++)
        {
            this->insertAlways( GMlib::TSVertex<T>( GMlib::Point<T,2>(workingVector + center) ) );
            workingVector = rotationMatrix * workingVector;

            if (i < rings - 1)
            {
                Node<T> newNode = Node<T>(this->getVertex(this->getSize() - 1));
                this->addElement(newNode);
            }
        }

    }

}


//IRREGULAR POINT GENERATOR FOR DRUM
template <typename T>
void Drum<T>::irreg(int triangles, T radius)
{

    GMlib::SqMatrix<T,2> rotationMatrix;
    GMlib::Vector<T,2> rotationVector {radius, 0.0f};

    float s = std::sqrt( (2 * M_2PI * ( radius * radius )) / ( std::sqrt(3) * triangles ) );
    float k = (M_2PI * radius) / s;
    int n = (triangles + 2 + k) / 2;
    float h = 2 * (radius -  (radius * std::cos(GMlib::Angle ( .5 * (M_2PI / k) ).getRad()) ));
    auto radiusRandom = radius - std::fabs(h);

    rotationMatrix = GMlib::SqMatrix<T,2>( GMlib::Angle ( M_2PI / k) );

    for (int i = 0; i < k; i++)
    {
        this->insertAlways( GMlib::TSVertex<T>( GMlib::Point<T,2>(rotationVector + center) ) );
        rotationVector = rotationMatrix * rotationVector;
    }

    for (int i = 0; i < (n-k); i++)
    {
        float x1;
        float y1;
        float x2;
        float y2;
        bool check = true;
        bool checkLength = true;
        GMlib::Vector<float,2> len;

        while (check)
        {

            do
            {
                x1 = radiusRandom * GMlib::Random<T>(-1, 1).get();
                y1 = radiusRandom * GMlib::Random<T>(-1, 1).get();
                len[0] = x1;
                len[1] = y1;
            }while(len.getLength() >= radiusRandom);

            for (int i = 0; i < this->getSize(); i++)
            {
                x2 = this->getVertex(i)->getPosition()[0];
                y2 = this->getVertex(i)->getPosition()[1];

                if ( std::fabs((GMlib::Point<float,2>( (x1), (y1) ) - GMlib::Point<float,2>(x2, y2)).getLength()) < (2*s/3))
                {
                    checkLength = false;
                }
            }
            if (checkLength == false)
            {
                checkLength = true;
                check = true;
            }
            else
            {
                check = false;
            }

        }

        this->insertAlways(GMlib::TSVertex<T>( GMlib::Point<T,2>( this->center[0] + (x1), this->center[1] + (y1) ) ) );

        Node<T> newNode = Node<T>(this->getVertex(this->getSize() - 1));
        this->addElement(newNode);

    }

}

//ADDS NODES THAT ARE 'ELEMENTS'
template <typename T>
void Drum<T>::addElement(Node<T> center)
{
    this->element.addNode(center);
}

//STEPS FOR CALCULATING STIFFNESS MATRIX
template <typename T>
void Drum<T>::calculateStiffnessMatrix()
{
    this->element.prepare();
    this->element.calculateStiffnessLoad();


//    std::cout << this->element.getStiffnessMatrix() << std::endl;
//    std::cout << this->element.getLoadVector() << std::endl;
//    std::cout << this->element.getInvertedStiffnessMatrix() << std::endl;
//    std::cout << this->element.getXVector() << std::endl;

}

//LOCAL SIMULATE - HANDLES DRUM OSCILLATION
template <typename T>
void Drum<T>::localSimulate(double dt)
{


    if (bool1)
    {
        force += dt;
        if (this->force > 1.5f)
        {
            bool1 = false;
        }
    }
    else
    {
        force -= dt;
        if (this->force < -1.5f)
        {
            bool1 = true;
        }
    }

    this->element.calculateXVector(this->force);

    for (int i = 0; i < this->element.getNodes().size(); i++)
    {
        this->element.getNodes()[i].setZValue(this->element.getXVector()[i]);
    }

    this->replot();
 }

//HELPER FUNCTION - CALLS X CALCULATION IN ELEMENT OBJECT
template <typename T>
void Drum<T>::calculateX()
{
    this->element.calculateXVector();
}

//SETS CENTER POINT OF DRUM
template <typename T>
void Drum<T>::setCenter(GMlib::Point<T,2> newCenter)
{
    this->center[0] = newCenter[0];
    this->center[1] = newCenter[1];
}

#endif

