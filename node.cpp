#ifdef NODE_H

//DEFAULT CONSTRUCTOR
template <typename T>
Node<T>::Node()
{

}

//CONSTRUCTOR WITH VERTEX POINTER
template <typename T>
Node<T>::Node(GMlib::TSVertex<T>* vertex)
{
    this->vertex = vertex;
}

//DESTRUCTOR
template <typename T>
Node<T>::~Node()
{

}

//GETTER FOR VERTEX
template <typename T>
GMlib::TSVertex<T> Node<T>::getVertex()
{
    return *(this->vertex);
}

//CHECKS IF TWO VERTICES ARE NEIGHBORS / SHARE AN EDGE
template <typename T>
bool Node<T>::isNeighbor(Node<T> neighbor)
{
    for (int i = 0; i < this->getVertex().getEdges().getSize(); i++)
    {
        if (neighbor.getVertex() == *( this->getVertex().getEdges()[i]->getOtherVertex(this->getVertex())))
        {
            return true;
        }
    }
}

//RETURNS THE EDGES THAT ARE NOT ALONG THE EDGES OF THE DRUM
template <typename T>
GMlib::TSEdge<T>* Node<T>::getNeighborEdge(Node<T> neighbor)
{
    for (int i = 0; i < this->getVertex().getEdges().getSize(); i++)
    {
        if (neighbor.getVertex() == *(this->getVertex().getEdges()[i]->getOtherVertex(this->getVertex())))
        {
            return this->getVertex().getEdges()[i];
        }
    }
}

//GETS THE VERTICES FOR CALCULATING MATRIX NON-DIAGONALS BASED ON A GIVEN EDGE
template <typename T>
GMlib::ArrayLX<GMlib::TSVertex<T>*> Node<T>::getVectorVertices(GMlib::TSEdge<T>* edge)
{

    GMlib::ArrayLX<GMlib::TSVertex<T>*> returnArray;

    returnArray.insertAlways(this->vertex);
    returnArray.insertAlways(edge->getOtherVertex(this->getVertex()));

    for (int i = 0; i < edge->getTriangle().getSize(); i++)
    {
        for (int j = 0; j < edge->getTriangle()[i]->getVertices().getSize(); j++)
        {
            if (!returnArray.exist(edge->getTriangle()[i]->getVertices()[j]))
            {
                returnArray.insertAlways(edge->getTriangle()[i]->getVertices()[j]);
            }
        }
    }

    return returnArray;

}

//GETS THE VERTICES FOR CALCULATING MATRIX DIAGONAL BASED ON A GIVEN TRIANGLE
template <typename T>
GMlib::ArrayLX<GMlib::TSVertex<T>*> Node<T>::getDiagonalVertices(GMlib::TSTriangle<T>* triangle)
{
    GMlib::ArrayLX<GMlib::TSVertex<T>*> returnArray;

    returnArray.insertAlways(this->getVertexRef());

    for (int i = 0; i < triangle->getEdges().getSize(); i++)
    {
        if (triangle->getEdges()[i]->getOtherVertex(this->getVertex()) != 0 )
        {
            returnArray.insertAlways(triangle->getEdges()[i]->getOtherVertex(this->getVertex()));
        }
    }

    return returnArray;
}

//SETS THE Z VALUE FOR THE VERTEX
template <typename T>
void Node<T>::setZValue(T zValue)
{
    this->getVertexRef()->setZ(zValue);
}

//GETTER FOR REFERENCE TO THE VERTEX
template <typename T>
GMlib::TSVertex<T>* Node<T>::getVertexRef()
{
    return this->vertex;
}

#endif
