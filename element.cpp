
#ifdef ELEMENT_H

//DEFAULT CONSTRUCTOR
template <typename T>
Element<T>::Element()
{

}

//FUNCTION TO CALCULATE STIFFNESS MATRIX AND INITIAL LOAD VECTOR
template <typename T>
void Element<T>::calculateStiffnessLoad()
{


    GMlib::ArrayLX<GMlib::TSVertex<T>*> vertexNonDiagonalArray;
    GMlib::Vector<T,2> d;
    GMlib::Vector<T,2> a1;
    GMlib::Vector<T,2> a2;
    float dd;
    float area1;
    float dh1;
    float h1;
    float area2;
    float dh2;
    float h2;

    GMlib::ArrayLX<GMlib::TSVertex<T>*> vertexDiagonalArray;
    GMlib::Vector<T,2> d1;
    GMlib::Vector<T,2> d2;
    GMlib::Vector<T,2> d3;
    float Tk = 0.0f;
    float volTk = 0.0f;
    float plusTk = 0.0f;
    float plusVolTk = 0.0f;


    for (int i = 0; i < this->nodes.size(); i++)
    {
        for (int j = i+1; j < this->nodes.size(); j++)
        {

            if(this->nodes.at(i).isNeighbor(this->nodes.at(j)))
            {

                GMlib::TSEdge<T>* neighborEdge = this->nodes.at(i).getNeighborEdge(this->nodes.at(j));
                vertexNonDiagonalArray = this->nodes.at(i).getVectorVertices(neighborEdge);

                d = vertexNonDiagonalArray[1]->getPosition() - vertexNonDiagonalArray[0]->getPosition();
                a1 = vertexNonDiagonalArray[2]->getPosition() - vertexNonDiagonalArray[0]->getPosition();
                a2 = vertexNonDiagonalArray[3]->getPosition() - vertexNonDiagonalArray[0]->getPosition();

                dd = 1 / (d*d);
                area1 = std::abs(d ^ a1);
                dh1 = dd * (a1 * d);
                h1 = dd * area1 * area1;
                area2 = std::abs(d ^ a2);
                dh2 = dd * (a2 * d);
                h2 = dd * area2 * area2;

                auto Aij = ((((dh1 * (1 - dh1)) / h1) - dd) * (area1/2)) + ((((dh2 * (1 - dh2)) / h2) - dd) * (area2/2));

                //std::cout << "Aij: " << Aij << std::endl << std::endl;

                this->stiffnessMatrix[i][j] = Aij;
                this->stiffnessMatrix[j][i] = Aij;
            }


            else
            {
                this->stiffnessMatrix[i][j] = 0;
                this->stiffnessMatrix[j][i] = 0;
            }

        }

        GMlib::TSTriangle<T>* workingTriangle;

        for (int k = 0; k < this->nodes.at(i).getVertex().getTriangles().getSize(); k++)
        {
            workingTriangle = this->nodes.at(i).getVertex().getTriangles()[k];
            vertexDiagonalArray = this->nodes.at(i).getDiagonalVertices(workingTriangle);

            d1 = vertexDiagonalArray[2]->getPosition() - vertexDiagonalArray[0]->getPosition();
            d2 = vertexDiagonalArray[1]->getPosition() - vertexDiagonalArray[0]->getPosition();
            d3 = vertexDiagonalArray[2]->getPosition() - vertexDiagonalArray[1]->getPosition();

            plusTk = ((d3 * d3) / (2 * std::abs(d1 ^ d2)));

            Tk += plusTk;
            volTk += (std::abs(d1 ^ d2)/6);
        }

        //std::cout << std::endl << std::endl;

        this->stiffnessMatrix[i][i] = Tk;
        this->loadVector[i] = volTk;
        Tk = 0;
        volTk = 0;
    }

    this->invertStiffness();

}

//INITIALIZES STIFFNESS MATRIX AND LOAD VECTOR AS THEY ARE DYNAMIC
template <typename T>
void Element<T>::prepare()
{
    this->stiffnessMatrix.setDim(this->nodes.size(), this->nodes.size());
    this->loadVector.setDim(this->nodes.size());
}

//ADD NODE ('ELEMENT')
template <typename T>
void Element<T>::addNode(Node<T> newNode)
{
    this->nodes.push_back(newNode);
}

//FUNCTION TO INVERT STIFFNESS MATRIX
template <typename T>
void Element<T>::invertStiffness()
{
    this->invertedStiffnessMatrix = this->getStiffnessMatrix();
    this->invertedStiffnessMatrix.invert();
}

//GETTER FOR STIFFNESS MATRIX
template <typename T>
GMlib::DMatrix<T> Element<T>::getStiffnessMatrix()
{
    return this->stiffnessMatrix;
}

//GETTER FOR NODES ('ELEMENTS')
template <typename T>
std::vector<Node<T>> Element<T>::getNodes()
{
    return this->nodes;
}

//GETTER FOR LOAD VECTOR
template <typename T>
GMlib::DVector<T> Element<T>::getLoadVector()
{
    return this->loadVector;
}

//GETTER FOR INVERTED STIFFNESS MATRIX
template <typename T>
GMlib::DMatrix<T> Element<T>::getInvertedStiffnessMatrix()
{
    return this->invertedStiffnessMatrix;
}

//GETTER FOR X VECTOR
template <typename T>
GMlib::DVector<T> Element<T>::getXVector()
{
    return this->xVector;
}

//CALCULATE X VECTOR BASED ON FORCE PARAMETER
template <typename T>
void Element<T>::calculateXVector(float force)
{
    this->xVector = this->getInvertedStiffnessMatrix() * (force * this->getLoadVector());
}



#endif


