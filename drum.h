#ifndef FEM_H
#define FEM_H

#include <gmCoreModule>
#include <gmTrianglesystemModule>
#include "node.h"
#include "element.h"
#include "memory"

template <typename T>
class Drum : public GMlib::TriangleFacets<T>
{
public:
    using GMlib::TriangleFacets<T>::TriangleFacets;


    //Functions for generating a regular or irregular set of points for the drum
    void reg(int rings, int points, T radius);
    void irreg(int triangles, T radius);

    //Intermediate function, to add a node to element for calculation
    void addElement(Node<T> newNode);

    //Intermediate functions, calling Element object calculations
    void calculateStiffnessMatrix();
    void calculateX();
    void calculateNewZValues();

    //Function to set the center point for the drum
    void setCenter(GMlib::Point<T,2>);

protected:
    //localSimulate inheritted function
    void localSimulate(double dt) override;

private:
    GMlib::Point<T,2> center {0.0, 0.0};
    Element<T> element;
    float force {0.0f};
    bool bool1 {true};

};

#include "drum.cpp"

#endif // FEM_H
